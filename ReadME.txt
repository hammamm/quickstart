Larval project
the source of this summary
https://laravel.com/docs/5.1/quickstart

*the project name is quickstart

1. create project     =>    Larval new quick start
2. Create migration with the table   =>   php artisan make:migration creaate_tasks_table —create = tasks
*the name of migration will be CreaTetasksTable and the name of table will be tasks*
3. Add column to table   =>   $table->string(‘name’);
*name of column = name. &  type of column = string*
4. Change setting in .env file as DB_DATABASE => quickstart   &  DB_USERNAME  =>  root & DB_PASSWORD =>*****
*database name and password as your info in mysql*
5. enter mysql  =>.  Mysql -u root -p***
6. Create database in mysql.  => create database quickstart;
*the name should be as your database. Name in .env file*
7. Migrate database from project to mysql database.  =>.  php artisan migrate
*with any new migration you need to do this command to migrate to mysql*
8. Add your routes in routes/web.php file  => Route::http methode(‘url’ , function(){});
for example  Route::get( ‘/‘ , function(){} );
9. Create view in resources/views folder  and name it as  tasks.blade.php
*tasks is the name of the view *
10. Inside the function of route return the view.  =>.  return view(‘tasks’)
11. To add layouts in html tamplate that allow you to get the page content any where =>. @yield(‘content’).             *this is parent page and child page  will be inherited from it . It will be written in blade template so you can call this page from another page*
12.   When ever you need the parent page in the child page you call it by extends(pageneam)
13. To store data make new metode store in controller then validate data fitst then store it
 Route::post('/task', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $task = new Task;
       $task->name = $request->name;
       $task->save();

       return redirect('/');
       });
 14. to delete data include form with hidden input that has delete methode then in delete it in Controller
 botun in view :
 <form action="/task/{{ $task->id }}" method="POST">
           {{ csrf_field() }}
           {{ method_field('DELETE') }}

           <button>Delete Task</button>
       </form>
  deleting in controller:
  Route::delete('/task/{id}', function ($id) {
    Task::findOrFail($id)->delete();

    return redirect('/');
});
