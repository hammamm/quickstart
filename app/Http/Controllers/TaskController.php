<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Validator;

class TaskController extends Controller
{
    public function index(Request $request)
    {
      $tasks = Task::orderBy('created_at', 'asc')->get();

      return view('tasks', [
          'tasks' => $tasks
      ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $task = new Task;
        $task->name = $request->name;
        $task->save();

        return redirect('/');
    }


    public function delete($id)
    {
        Task::findOrFail($id)->delete();

        return redirect('/');
    }

}
